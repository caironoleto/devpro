# Devpro

For this challenge, I started a new ruby 3.3.0 application with rspec, CSV, robocop, and active support gems.

I'm using the Devpro class as the main class. This class is responsible for calling the #solution method. 

To solve this challenge, I'm using Album and Photo objects, and given the restrictions, I'm using the insertion sort algorithm to order the photos by the given datetime.
