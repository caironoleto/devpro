# frozen_string_literal: true

require "album"
require "csv"
require "photo"

# Main class that runs the solution
class Devpro
  def solution(input)
    input = remove_invalid_chars(input)
    photos = to_csv(input)
    photos_with_album = photos.map.with_index do |photo_row, index|
      photo = Photo.new(index, photo_row)

      add_photo_to_album(photo)
      photo
    end

    photos_with_album.map do |photo|
      get_photo_filename(photo)
    end.join("\n")
  end

  private

  def add_photo_to_album(photo)
    @albums ||= {}

    album = album(photo)
    album.add_photo(photo)
  end

  def album(photo)
    album_name = photo.city.strip
    @albums[album_name] ||= Album.new(album_name)
  end

  def get_photo_filename(photo)
    album(photo).photo_filename(photo)
  end

  def remove_invalid_chars(input)
    input.dup.gsub!("\"", "")
  end

  def to_csv(input)
    CSV.parse(input)
  end
end
