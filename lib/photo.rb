# frozen_string_literal: true

class Photo
  attr_reader :id, :taken_at
  attr_accessor :album, :city

  def initialize(id, line)
    @id = id
    @filename, @city, self.taken_at = line
  end

  def extension
    File.extname(@filename)
  rescue TypeError
    ""
  end

  def taken_at=(value)
    @taken_at = DateTime.parse(value)
  rescue TypeError, Date::Error
    nil
  end
end
