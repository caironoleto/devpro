# frozen_string_literal: true

require "active_support/all"

class Album
  attr_reader :name, :photos

  def initialize(name)
    raise ArgumentError, "Album name cannot be blank" if name.blank?

    @name = name
    @photos ||= []
    @pages ||= {}
  end

  def add_photo(photo)
    photo.album = self

    order_photos!(photo)
  end

  def photo_filename(photo)
    page = @pages[photo.id].to_s.rjust(@photos.length.to_s.length, "0")

    "#{@name}#{page}#{photo.extension}"
  end

  private

  def order_photos!(photo)
    prepend_photo(photo)

    1...@photos.length.times.each do |current_page|
      page = current_page

      while page.positive?
        break unless previous_photo_greater_than?(page)

        swap_previous_photo_with_current_photo!(page)

        page -= 1
      end

      set_photos_page!(current_page)
    end
  end

  def prepend_photo(photo)
    @photos.prepend(photo)
    @pages[photo.id] = 1
  end

  def previous_photo_greater_than?(page)
    @photos[page - 1].taken_at > @photos[page].taken_at
  end

  def replace_photo(photo, page)
    @photos[page] = photo
    previous = @photos[page - 1]

    @pages[photo.id] = @pages[previous.id] + 1
  end

  def set_photos_page!(current_page)
    previous_photo = @photos[current_page - 1]
    @pages[previous_photo.id] = current_page

    current_photo = @photos[current_page]
    @pages[current_photo.id] = current_page + 1
  end

  def swap_previous_photo_with_current_photo!(page)
    previous = @photos[page - 1]
    current = @photos[page]

    replace_photo(current, page - 1)
    replace_photo(previous, page)
  end
end
