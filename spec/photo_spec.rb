# frozen_string_literal: true
require "spec_helper"

RSpec.describe Photo do
  describe ".new" do
    it "sets the taken_at attribute" do
      photo = Photo.new(0, ["photo.jpg", " Krakow", "2013-09-05 14:08:15"])

      expect(photo.taken_at).to eq(DateTime.new(2013, 9, 5, 14, 8, 15))
    end
  end

  describe "#extension" do
    it "returns the extension of the photo" do
      photo = Photo.new(0, ["photo.jpg", " Krakow", "2013-09-05 14:08:15"])

      expect(photo.extension).to eq(".jpg")
    end

    it "returns an empty string when there is no extension" do
      photo = Photo.new(0, ["photo", " Krakow", "2013-09-05 14:08:15"])

      expect(photo.extension).to eq("")
    end

    it "returns an empty string when filename is invalid" do
      photo = Photo.new(0, [nil, " Krakow", "2013-09-05 14:08:15"])

      expect(photo.extension).to eq("")
    end
  end

  describe "#taken_at=" do
    subject(:photo) { Photo.new(0, ["photo.jpg", " Krakow", "2013-09-05 14:08:15"]) }

    it "sets the given string" do
      photo.taken_at = "2013-09-06 14:08:15"

      expect(photo.taken_at).to eq(DateTime.new(2013, 9, 6, 14, 8, 15))
    end

    it "does not raise error with invalid input" do
      expect {
        photo.taken_at = nil
      }.to_not raise_error
    end

    it "does not assign with invalid input" do
      photo.taken_at = nil

      expect(photo.taken_at).to eq(DateTime.new(2013, 9, 5, 14, 8, 15))
    end
  end
end
