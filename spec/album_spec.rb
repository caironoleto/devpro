# frozen_string_literal: true
require "spec_helper"

RSpec.describe Album do
  subject(:album) { Album.new("Krakow") }
  let(:invalid_album) { Album.new(nil) }
  let(:photo) { Photo.new(0, ["photo.jpg", " Krakow", "2013-09-05 14:08:15"]) }

  describe ".new" do
    it "creates a new album" do
      expect(album.name).to eq("Krakow")
    end

    it "initializes photos to an empty array" do
      expect(album.photos).to eq([])
    end

    it "raises an error when name is invalid" do
      expect { invalid_album }.to raise_error(ArgumentError)
    end
  end

  describe "#add_photo" do
    it "adds a photo to the album" do
      album.add_photo(photo)

      expect(album.photos).to include(photo)
    end

    it "sets the album property on the photo" do
      album.add_photo(photo)

      expect(photo.album).to eq(album)
    end

    it "orders photos by date" do
      expected_photos = (0..11).map do |i|
        photo = Photo.new(i, ["photo#{i}.jpg", " Krakow", "2013-09-01 #{i.to_s.rjust(2, "0")}:08:15"])
      end

      expected_photos.shuffle.shuffle.each do |photo|
        album.add_photo(photo)
      end

      expect(album.photos).to eq(expected_photos)
    end
  end

  describe "#photo_filename" do
    it "returns the photo filename using the City's name and the position of the photo on sorted photos" do
      album.add_photo(photo)

      expect(album.photo_filename(photo)).to eq("Krakow1.jpg")
    end

    it "returns the photo filename with 0 on the left when has more than 10 photos from the same album" do
      0..11.times do |i|
        photo = Photo.new(i, ["photo#{i}.jpg", " Krakow", "2013-09-01 #{i.to_s.rjust(2, "0")}:08:15"])
        album.add_photo(photo)
      end

      photo = Photo.new(20, ["photo-20.jpg", " Krakow", "2013-09-02 14:08:15"])
      album.add_photo(photo)

      expect(album.photo_filename(photo)).to eq("Krakow12.jpg")
    end
  end
end
